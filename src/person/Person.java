package person;

import java.util.List;

public class Person {
	
	private int alter ;
	private final Geschlecht geschlecht;
	private final String vorname;
	private final String name;
	private List<Adresse> adresse;
	
	public Person(int alter, Geschlecht geschlecht, 
			String vorname, String name) {
		this.setAlter(alter) ;
		this.geschlecht = geschlecht;
		this.vorname = vorname;
		this.name = name;
	}
	

	public List<Adresse> getAdresse() {
		return adresse;
	}

	public void setAdresse(List<Adresse> adresse) {
		this.adresse = adresse;
	}

	public String getVorname() {
		return vorname;
	}

	public Geschlecht getGeschlecht() {
		return geschlecht;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}

	public String getName() {
		return name;
	}


	@Override
	public String toString() {
		return "Person [alter=" + alter + ", geschlecht=" + geschlecht + ", vorname=" + vorname + ", name=" + name
				+ ", adresse=" + adresse + "]";
	}
	
	
}
