package person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.omg.Messaging.SyncScopeHelper;

public class Personenliste {
	List<Person> persons;

	Personenliste() {
		persons = new ArrayList<Person>();

		for (int i = 0; i < 10; i++) {
			initialize(i);
		}

	}

	private void initialize(int i) {
		Person tempPerson;
		Geschlecht tempGeschlecht;
		if (i % 2 == 0)
			tempGeschlecht = Geschlecht.MÄNNLICH;
		else
			tempGeschlecht = Geschlecht.WEIBLICH;

		tempPerson = new Person(i + 5, tempGeschlecht, "Test" + i, "Gang" + i);
		List<Adresse> adressListe = new ArrayList<Adresse>();

		adressListe = createAdress();
		tempPerson.setAdresse(adressListe);
		persons.add(tempPerson);
	}

	private List<Adresse> createAdress() {
		List<Adresse> tempListe = new ArrayList<Adresse>();
		for (int i = 0; i < 2; i++) {
			Adresse tempAdrese;
			if (i == 1) {
				tempAdrese = new Adresse(Adressart.HOME, "Asternweg", "79312", "VS");
			} else {
				tempAdrese = new Adresse(Adressart.FERIENHAUS, "Roseweg", "12313", "Kambodscha");

			}

			tempListe.add(tempAdrese);
		}

		return tempListe;

	}

	public static void main(String[] args) {

		Personenliste pl = new Personenliste();
		System.out.println("Aufgabe 1");
		pl.printAllNames();
		System.out.println("Aufgabe 2");
		System.out.println(pl.getAllFirstnames());
		System.out.println("Aufgabe 3");
		System.out.println(pl.getAllHomeAdresses());
		System.out.println("Aufgabe 5");
		pl.getOldestPerson();
		System.out.println("Aufgabe 6");
		System.out.println(pl.sortByAgeDesc());
		System.out.println("Aufgabe 7");
		System.out.println(pl.containsPoser());

	}

	private boolean containsPoser() {
		return persons
				.stream()
				.anyMatch(p -> p.getAlter() < 20 && p.getAdresse()
						.stream()
						.anyMatch(a -> a.getArt() == Adressart.FERIENHAUS));
				
	}

	private List<Person> sortByAgeDesc() {
		return persons
				.stream()
				.sorted((a1,a2) -> a2.getAlter() - a1.getAlter())
				.collect(Collectors.toList());
	}
	

	private Optional<Person> getOldestPerson() {

		return persons
				.stream()
				.reduce((p1,p2) -> p1.getAlter() > p2.getAlter() ? p1 : p2);
				
	}

	private List<String> getAllFirstnames() {
		return persons.stream() //
				.map(Person::getVorname) //
				.collect(Collectors.toList()); //

	}

	private List<Person> getAllFemales() {
		return persons.stream() //
				.filter(p -> p.getGeschlecht() == Geschlecht.WEIBLICH) //
				.collect(Collectors.toList()); //

	}

	private void printAllNames() {
		persons.stream()//
				.forEach(System.out::println); //
	}

	public List<Adresse> getAllHomeAdresses() {
		return persons //
				.stream()
				.flatMap(p -> p.getAdresse().stream())
				.filter(a -> a.getArt() == Adressart.HOME)
				.collect(Collectors.toList());
	}

	@Override
	public String toString() {
		
		return "Personenliste [persons=" + persons.toString() + "]";
	}

	
}
