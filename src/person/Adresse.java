package person;

public class Adresse {
	private final Adressart art;
	private final String strasse;
	private final String plz;
	private final String ort;
	public Adresse(Adressart art, String strasse, String plz, String ort) {
		this.art = art;
		this.strasse = strasse;
		this.plz = plz;
		this.ort = ort;
	}
	public Adressart getArt() {
		return art;
	}
	public String getStrasse() {
		return strasse;
	}
	public String getPlz() {
		return plz;
	}
	public String getOrt() {
		return ort;
	}
	@Override
	public String toString() {
		return "Adresse [art=" + art + ", strasse=" + strasse + ", plz=" + plz + ", ort=" + ort + "]";
	}
	
	
}
